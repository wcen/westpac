package nz.co.westpac

import nz.co.westpac.models.Post
import nz.co.westpac.models.Repository
import nz.co.westpac.ui.PostListAdapter
import org.junit.Assert.*
import org.junit.Test

class UnitTest {

    @Test
    fun repository_updateCachedPost_updateExistingPost() {

        val repo = Repository()
        repo.posts = mutableListOf<Post>(Post(1, 1, "1", "1"))
        val updatedPost = Post(1, 2, "2", "2")
        assertNotNull(repo.updateCachedPost(updatedPost))
        assertTrue(repo.posts.size == 1
                && repo.posts[0].userId == updatedPost.userId
                && repo.posts[0].title == updatedPost.title
                && repo.posts[0].body == updatedPost.body)
    }

    @Test
    fun repository_updateCachedPost_updateNonExistingPost() {

        val repo = Repository()
        repo.posts = mutableListOf<Post>(Post(1, 1, "1", "1"))
        val updatedPost = Post(2, 2, "2", "2")
        assertNull(repo.updateCachedPost(updatedPost))
        assertTrue(repo.posts.size == 1
                && repo.posts[0].userId != updatedPost.userId
                && repo.posts[0].title != updatedPost.title
                && repo.posts[0].body != updatedPost.body)
    }

    @Test
    fun postListAdapter_updatePost_updateExistingPost() {

        val adapter = PostListAdapter(mutableListOf<Post>(Post(1, 1, "1", "1")), null)
        val updatedPost = Post(1, 2, "2", "2")
        assertNotNull(adapter.updatePostList(updatedPost))
        assertTrue(adapter.posts.size == 1
                && adapter.posts[0].userId == updatedPost.userId
                && adapter.posts[0].title == updatedPost.title
                && adapter.posts[0].body == updatedPost.body)
    }

    @Test
    fun postListAdapter_updatePost_updateNonExistingPost() {

        val adapter = PostListAdapter(mutableListOf<Post>(Post(1, 1, "1", "1")), null)
        val updatedPost = Post(2, 2, "2", "2")
        assertNull(adapter.updatePostList(updatedPost))
        assertTrue(adapter.posts.size == 1
                && adapter.posts[0].userId != updatedPost.userId
                && adapter.posts[0].title != updatedPost.title
                && adapter.posts[0].body != updatedPost.body)
    }
}