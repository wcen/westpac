package nz.co.westpac

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.matcher.BoundedMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher


object CustomMatcher {
    fun recyclerViewHasMoreThanNItems(n: Int): Matcher<View?>? {
        return object : TypeSafeMatcher<View?>() {
            override fun matchesSafely(view: View?): Boolean {
                return view is RecyclerView && view.adapter!!.itemCount > n
            }

            override fun describeTo(description: Description) {}
        }
    }
}