package nz.co.westpac

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import nz.co.westpac.ui.PostListAdapter
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PostListTest: BaseTestTemplate() {

    @Test
    fun screenElementsTest() {
        Thread.sleep(Constants.WAIT_TIME_MILLISECOND_LONG)
        Espresso.onView(withText(R.string.posts))
            .check(ViewAssertions.matches(isDisplayed()))

        Espresso.onView(withId(R.id.postRecyclerView))
            .check(ViewAssertions.matches(isDisplayed()))

        Espresso.onView(withId(R.id.postRecyclerView))
            .check(ViewAssertions.matches(CustomMatcher.recyclerViewHasMoreThanNItems(0)))

        Espresso.onView(withId(R.id.postRecyclerView))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<PostListAdapter.ViewHolder>(
                    0,
                    ViewActions.click()
                )
            )

        Thread.sleep(Constants.WAIT_TIME_MILLISECOND_LONG)
        Espresso.onView(withText(R.string.postDetail))
            .check(ViewAssertions.matches(isDisplayed()))
    }
}