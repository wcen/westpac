package nz.co.westpac

import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import nz.co.westpac.ui.PostListAdapter
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PostDetailsTest: BaseTestTemplate() {

    @Test
    fun screenElementsTest() {
        Thread.sleep(Constants.WAIT_TIME_MILLISECOND_LONG)
        onView(ViewMatchers.withId(R.id.postRecyclerView))
            .perform(RecyclerViewActions.actionOnItemAtPosition<PostListAdapter.ViewHolder>(0, ViewActions.click()))

        Thread.sleep(Constants.WAIT_TIME_MILLISECOND_LONG)

        onView(ViewMatchers.withText(R.string.postDetail))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.title))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withId(R.id.postBody))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withId(R.id.postCommentRecyclerView))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.postCommentRecyclerView))
            .check(ViewAssertions.matches(CustomMatcher.recyclerViewHasMoreThanNItems(0)))

    }

    @Test
    fun screenNavigationTest() {
        Thread.sleep(Constants.WAIT_TIME_MILLISECOND_LONG)
        onView(ViewMatchers.withText(R.string.posts))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.postRecyclerView))
            .perform(RecyclerViewActions.actionOnItemAtPosition<PostListAdapter.ViewHolder>(0, ViewActions.click()))

        Thread.sleep(Constants.WAIT_TIME_MILLISECOND_LONG)

        onView(ViewMatchers.withText(R.string.postDetail))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withContentDescription("Navigate up")).perform(ViewActions.click())

        onView(ViewMatchers.withText(R.string.posts))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.postRecyclerView))
            .perform(RecyclerViewActions.actionOnItemAtPosition<PostListAdapter.ViewHolder>(0, ViewActions.click()))

        Thread.sleep(Constants.WAIT_TIME_MILLISECOND_LONG)

        onView(ViewMatchers.withText(R.string.postDetail))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        val mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        mDevice.pressBack()

        onView(ViewMatchers.withText(R.string.posts))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}