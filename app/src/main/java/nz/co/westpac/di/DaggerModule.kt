package nz.co.westpac.di

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import nz.co.westpac.App
import nz.co.westpac.ui.MainActivity
import nz.co.westpac.ui.PostDetailsActivity
import nz.co.westpac.viewmodels.MainViewModel
import nz.co.westpac.viewmodels.PostDetailsViewModel
import nz.co.westpac.viewmodels.ViewModelFactory

@Module
class DaggerModule {
    @Provides
    fun provideMainViewModel(activity: MainActivity, app: App): MainViewModel {
        return ViewModelProvider(activity, ViewModelFactory(app))
            .get(MainViewModel::class.java)
    }

    @Provides
    fun providePostDetailsViewModel(activity: PostDetailsActivity, app: App): PostDetailsViewModel {
        return ViewModelProvider(activity, ViewModelFactory(app))
            .get(PostDetailsViewModel::class.java)
    }
}
