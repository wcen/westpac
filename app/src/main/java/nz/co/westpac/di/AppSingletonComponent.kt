package nz.co.westpac.di

import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import nz.co.westpac.App
import nz.co.westpac.network.RetrofitClient
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        // Android internal module
        AndroidSupportInjectionModule::class,
        // App level modules
        AppSingletonDaggerModule::class,
        //      UI modules
        DaggerUIModule::class
    ]
)
interface AppSingletonComponent {
    fun inject(app: App)
    fun app(): App
    fun retrofitClient(): RetrofitClient
}
