package nz.co.westpac.di

import dagger.Module
import dagger.Provides
import nz.co.westpac.App
import javax.inject.Singleton

@Module
class AppSingletonDaggerModule(val app: App) {

    @Singleton
    @Provides
    fun provideApplication() = app

}
