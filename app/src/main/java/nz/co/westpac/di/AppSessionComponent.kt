package nz.co.westpac.di

import dagger.Component
import nz.co.westpac.models.Repository
import nz.co.westpac.viewmodels.MainViewModel
import nz.co.westpac.viewmodels.PostDetailsViewModel

@SessionScope
@Component(
    dependencies = [AppSingletonComponent::class]
)
interface AppSessionComponent {
    fun inject(mainViewModel: MainViewModel)
    fun inject(postDetailsViewModel: PostDetailsViewModel)
}
