package nz.co.westpac.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import nz.co.westpac.ui.MainActivity
import nz.co.westpac.ui.PostDetailsActivity

@Module
abstract class DaggerUIModule {
    @ContributesAndroidInjector(modules = [(DaggerModule::class)])
    abstract fun contributesMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [(DaggerModule::class)])
    abstract fun contributesPostDetailsActivity(): PostDetailsActivity
}