package nz.co.westpac.viewmodels

import androidx.lifecycle.AndroidViewModel
import nz.co.westpac.App
import nz.co.westpac.models.Comment
import nz.co.westpac.models.Post
import nz.co.westpac.models.Repository
import java.lang.Exception
import javax.inject.Inject

class PostDetailsViewModel(app: App) : BaseViewModel(app) {

    @Inject
    lateinit var repository: Repository

    init {
        App.appSessionComponent.inject(this)
    }

    fun getPostBy(postId: Int): Post? {
        return repository.getPostBy(postId)
    }

    suspend fun fetchCommentFor(post: Post): List<Comment>? {
        return try {
            repository.fetchCommentsFor(post)
        } catch (e: Exception) {
            e.printStackTrace()
            errorData.value = e
            null
        }
    }
}