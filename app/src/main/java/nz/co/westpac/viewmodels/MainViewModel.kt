package nz.co.westpac.viewmodels

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import nz.co.westpac.App
import nz.co.westpac.models.Post
import nz.co.westpac.models.Repository
import java.lang.Exception
import javax.inject.Inject

class MainViewModel(app: App) : BaseViewModel(app) {

    @Inject
    lateinit var repository: Repository

    val postsLiveData: MutableLiveData<List<Post>> get() {
        return repository.postListLiveData
    }

    val updatePostLiveData: MutableLiveData<Post> get() {
        return repository.updatePostLiveData
    }

    init {
        App.appSessionComponent.inject(this)
    }

    suspend fun fetchPosts() {
        try {
            repository.fetchPosts()
        } catch (e: Exception) {
            e.printStackTrace()
            errorData.value = e
        }
    }

    suspend fun fetchPostDetails(post: Post) {
        try {
            repository.fetchPostDetails(post)
        } catch (e: Exception) {
            e.printStackTrace()
            errorData.value = e
        }
    }
}