package nz.co.westpac.viewmodels

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import nz.co.westpac.App

open class BaseViewModel(app: App) : AndroidViewModel(app) {

    val errorData = MutableLiveData<Exception>()

}