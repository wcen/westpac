package nz.co.westpac.network

import nz.co.westpac.models.Comment
import nz.co.westpac.models.Post
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import java.util.*

interface Services {

    @GET("posts")
    suspend fun fetchPosts(): List<Post>

    @GET("posts/{postId}")
    suspend fun fetchPostDetails(@Path("postId") postId: Int): Post

    @GET("posts/{postId}/comments")
    suspend fun fetchCommentsFor(@Path("postId") postId: Int): List<Comment>
}