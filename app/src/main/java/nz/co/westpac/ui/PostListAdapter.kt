package nz.co.westpac.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import nz.co.westpac.databinding.PostListItemBinding
import nz.co.westpac.models.Post

open class PostListAdapter(var posts: List<Post>, private val listener: PostSelectionListener?)
    : RecyclerView.Adapter<PostListAdapter.ViewHolder>(){

    interface PostSelectionListener {
        fun onPostSelected(post: Post)
    }

    fun updatePost(post: Post) {
        updatePostList(post)?.let {
            notifyItemChanged(it)
        }
    }

    fun updatePostList(post: Post): Int? {
        val mutableList = posts.toMutableList()
        for (i in 0 until mutableList.size) {
            if (post.id == mutableList[i].id) {
                mutableList[i] = post
                posts = mutableList.toList()

                return i
            }
        }
        return null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = PostListItemBinding.inflate(layoutInflater,
            parent,
            false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val post = posts[position]
        viewHolder.bind(post)
        viewHolder.itemView.setOnClickListener {
            listener?.onPostSelected(post)
        }
    }

    override fun getItemCount(): Int {
        return posts.size
    }

    class ViewHolder(private val binding: PostListItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(post: Post) {
            binding.post = post
            binding.executePendingBindings()
        }
    }
}