package nz.co.westpac.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.coroutineScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.launch
import nz.co.westpac.R
import nz.co.westpac.models.Post
import nz.co.westpac.viewmodels.MainViewModel
import javax.inject.Inject

class MainActivity : AppCompatActivity(), PostListAdapter.PostSelectionListener {

    @Inject
    lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        title = getString(R.string.posts)

        swipeRefreshLayout.isEnabled = false

        postRecyclerView.apply {
            adapter = PostListAdapter(emptyList(), this@MainActivity)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        }

        viewModel.postsLiveData.observe(this, Observer {
            val adapter = postRecyclerView.adapter as PostListAdapter
            adapter.posts = it
            adapter.notifyDataSetChanged()
        })

        viewModel.updatePostLiveData.observe(this, Observer {
            (postRecyclerView.adapter as PostListAdapter).updatePost(it)
        })

        viewModel.errorData.observe(this, {
            Toast.makeText(this, R.string.errorMessage, Toast.LENGTH_LONG).show()
        })

        lifecycle.coroutineScope.launch {
            swipeRefreshLayout.isRefreshing = true
            viewModel.fetchPosts()
            swipeRefreshLayout.isRefreshing = false
        }
    }

    override fun onPostSelected(post: Post) {
        lifecycle.coroutineScope.launch {
            swipeRefreshLayout.isRefreshing = true
            viewModel.fetchPostDetails(post)
            swipeRefreshLayout.isRefreshing = false

            val intent = Intent(this@MainActivity, PostDetailsActivity::class.java)
            intent.putExtra(PostDetailsActivity.POST_ID_EXTRA_KEY, post.id)
            startActivity(intent)
            overridePendingTransition(R.anim.activity_open_translate, R.anim.stay)
        }
    }
}