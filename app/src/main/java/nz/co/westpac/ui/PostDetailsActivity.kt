package nz.co.westpac.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.coroutineScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_post_details.*
import kotlinx.coroutines.launch
import nz.co.westpac.R
import nz.co.westpac.databinding.ActivityPostDetailsBinding
import nz.co.westpac.models.Comment
import nz.co.westpac.models.Post
import nz.co.westpac.viewmodels.PostDetailsViewModel
import javax.inject.Inject

class PostDetailsActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel: PostDetailsViewModel

    private lateinit var binding: ActivityPostDetailsBinding

    private var postId = POST_ID_NOT_FOUND

    companion object {
        const val POST_ID_EXTRA_KEY = "postId"
        const val POST_ID_NOT_FOUND = -1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_post_details)

        title = getString(R.string.postDetail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        postCommentRecyclerView.apply {
            adapter = PostCommentListAdapter(listOf())
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

            val dividerItemDecoration = DividerItemDecoration(
                context,
                LinearLayoutManager.VERTICAL
            )
            addItemDecoration(dividerItemDecoration)
        }

        viewModel.errorData.observe(this, {
            Toast.makeText(this, R.string.errorMessage, Toast.LENGTH_LONG).show()
        })

        postId = intent.getIntExtra(POST_ID_EXTRA_KEY, POST_ID_NOT_FOUND)
        if (postId != POST_ID_NOT_FOUND) {
            viewModel.getPostBy(postId)?.let { post ->
                binding.post = post
                binding.executePendingBindings()
                fetchCommentsFor(post)
            }
        }
    }

    private fun fetchCommentsFor(post: Post) {
        lifecycle.coroutineScope.launch {
            viewModel.fetchCommentFor(post)?.let {
                val adapter = postCommentRecyclerView.adapter as PostCommentListAdapter
                adapter.comments = it
                adapter.notifyDataSetChanged()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        overridePendingTransition(R.anim.stay, R.anim.activity_close_translate)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}