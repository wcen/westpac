package nz.co.westpac.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import nz.co.westpac.databinding.CommentListItemBinding
import nz.co.westpac.models.Comment

class PostCommentListAdapter(var comments: List<Comment>):
    RecyclerView.Adapter<PostCommentListAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CommentListItemBinding.inflate(layoutInflater,
            parent,
            false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val comment = comments[position]
        viewHolder.bind(comment)
    }

    override fun getItemCount(): Int {
        return comments.size
    }

    class ViewHolder(private val binding: CommentListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(comment: Comment) {
            binding.comment = comment
            binding.executePendingBindings()
        }
    }
}