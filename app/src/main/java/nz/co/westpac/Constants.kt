package nz.co.westpac

object Constants {
    const val BASE_URL = "https://jsonplaceholder.typicode.com"
    //Connect timeout in seconds
    const val CONNECT_TIMEOUT = 60L
    //Read timeout in seconds
    const val READ_TIMEOUT = 60L

}