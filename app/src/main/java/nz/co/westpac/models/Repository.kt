package nz.co.westpac.models

import androidx.lifecycle.MutableLiveData
import nz.co.westpac.di.SessionScope
import javax.inject.Inject

@SessionScope
class Repository @Inject constructor() {

    @Inject
    lateinit var dataSource: DataSource

    var posts: MutableList<Post> = mutableListOf()

    val postListLiveData = MutableLiveData<List<Post>>()
    val updatePostLiveData = MutableLiveData<Post>()

    suspend fun fetchPosts() {
        posts = dataSource.fetchPosts().toMutableList()
        postListLiveData.value = posts
    }

    suspend fun fetchPostDetails(post: Post) {
        val updatedPost = dataSource.fetchPostDetails(post)
        updateCachedPost(updatedPost)?.let {
            updatePostLiveData.value = it
        }
    }

    fun updateCachedPost(updatedPost: Post): Post? {
        for (i in 0 until posts.size) {
            if (posts[i].id == updatedPost.id) {
                posts[i] = updatedPost
                return updatedPost
            }
        }
        return null
    }

    fun getPostBy(postId: Int): Post? {
        return posts.find { post -> post.id == postId }
    }

    suspend fun fetchCommentsFor(post: Post): List<Comment> {
        return dataSource.fetchCommentsFor(post)
    }
}