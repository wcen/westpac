package nz.co.westpac.models

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import nz.co.westpac.App
import nz.co.westpac.di.SessionScope
import nz.co.westpac.network.RetrofitClient
import nz.co.westpac.network.Services
import javax.inject.Inject

@SessionScope
class DataSource @Inject constructor(app: App, retrofitClient: RetrofitClient) {

    private val services: Services = retrofitClient.initRetrofit().create(Services::class.java)

    suspend fun fetchPosts(): List<Post> =
        networkAsyncCall {
            services.fetchPosts()
        }

    suspend fun fetchPostDetails(post: Post): Post =
        networkAsyncCall {
            services.fetchPostDetails(post.id)
        }

    suspend fun fetchCommentsFor(post: Post): List<Comment> =
        networkAsyncCall {
            services.fetchCommentsFor(post.id)
        }

    private suspend fun <T> networkAsyncCall(apiCall: suspend () -> T): T {
        return withContext(Dispatchers.IO) {
            try {
                apiCall.invoke()
            } catch (exception: java.lang.Exception) {
                throw exception
            }
        }
    }
}